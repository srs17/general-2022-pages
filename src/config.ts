export default {
	API_HOST: "https://general.srs-2022-assignment.workers.dev",
	MAX_IMAGE_WIDTH: 400,
	MAX_IMAGE_HEIGHT: 200,
};
